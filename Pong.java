
public class Pong {
	
	public static Panneau panneau = new Panneau ();
	public static Fenetre fenetre = new Fenetre ("Pong", 500, 500, true, panneau);
	public static Physique physique = new Physique ();
	public static Jeu jeu = new Jeu ();
	
	public static void main (String[] args) {
		panneau.addKeyListener (jeu);
		panneau.setFocusable (true);
		while (jeu.getPointsJoueur() < 5 && jeu.getPointsOrdi() < 5) {
			physique.mouvement();
			panneau.repaint();
			panneau.requestFocusInWindow();
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			while (jeu.getPause()) {
				panneau.requestFocusInWindow ();
			}
		}
	}

}
