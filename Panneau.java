import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class Panneau extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	JLabel score = new JLabel ();
	
	public void paintComponent(Graphics g) {
		
		//Fond d'écran
		g.setColor (Color.black);
		g.fillRect (
				0,
				0,
				Pong.fenetre.getLargeurFenetre (),
				Pong.fenetre.getHauteurFenetre ());
		
		//Ligne centrale
		g.setColor (Color.white);
		g.drawLine (
				Pong.fenetre.getLargeurFenetre ()/2,
				0,
				Pong.fenetre.getLargeurFenetre ()/2,
				Pong.fenetre.getHauteurFenetre ());
		
		//Balle
		g.fillOval (
				Pong.physique.getXBalle () - 10,
				Pong.physique.getYBalle () - 10,
				20,
				20);
		
		//Raquette du Joueur
		g.fillRect (
				10,
				Pong.jeu.getRaquetteJoueur () - Pong.jeu.getHauteurRaquetteJoueur(),
				10,
				2 * Pong.jeu.getHauteurRaquetteJoueur());
		
		//Raquette de l'ordinateur
		g.fillRect (
				Pong.fenetre.getLargeurFenetre() - 20,
				Pong.jeu.getRaquetteOrdi () - Pong.jeu.getHauteurRaquetteOrdi(),
				10,
				2 * Pong.jeu.getHauteurRaquetteOrdi());
	}
	public void afficherScore () {
		this.score.setForeground(Color.white);
		this.score.setText(""+Pong.jeu.getPointsJoueur()+"-"+Pong.jeu.getPointsOrdi());
		this.add (this.score);
	}
	
	
	
	public JSlider regleTailleRaquette = new JSlider ();
    public JSlider regleVitesseRaquette = new JSlider ();
    public JSlider regleVitesseXBalle = new JSlider ();
    public JSlider regleVitesseYBalle = new JSlider ();
    public JSlider regleAccelerationBalle = new JSlider ();
    public JLabel labelTailleRaquette = new JLabel ("Taille des raquettes :");
    public JLabel labelVitesseRaquette = new JLabel ("Vitesse des raquettes :");
    public JLabel labelVitesseXBalle = new JLabel ("Vitesse de la balle en x");
    public JLabel labelVitesseYBalle = new JLabel ("Vitesse de la balle en y");
    public JLabel labelAccelerationBalle = new JLabel ("Accélération de la balle");
    
    public void afficherMenu () {
    	
    	Pong.fenetre.setVisible(false);
        Pong.fenetre.setLayout (new GridLayout (10, 1));
        
        this.labelTailleRaquette.setOpaque(true);
        this.labelVitesseRaquette.setOpaque(true);
        this.labelVitesseXBalle.setOpaque(true);
        this.labelVitesseYBalle.setOpaque(true);
        this.labelAccelerationBalle.setOpaque(true);
        
        this.regleTailleRaquette.setMaximum (Pong.fenetre.getHauteurFenetre ());
        this.regleTailleRaquette.setValue (Pong.jeu.getHauteurRaquetteOrdi ());
        this.regleTailleRaquette.setPaintTicks (false);
        this.regleTailleRaquette.setPaintLabels (false);
        this.regleTailleRaquette.addChangeListener (new ChangeListener () {
        	public void stateChanged (ChangeEvent e) {
        		Pong.jeu.setHauteurRaquetteJoueur (Pong.panneau.regleTailleRaquette.getValue ());
        		Pong.jeu.setHauteurRaquetteOrdi (Pong.panneau.regleTailleRaquette.getValue ());
        		Pong.panneau.requestFocusInWindow ();
        	}
        });
        
        this.regleVitesseRaquette.setMaximum (1000);
        this.regleVitesseRaquette.setValue ((int) (100 * Pong.jeu.getVitesseRaquette ()));
        this.regleVitesseRaquette.setPaintTicks (false);
        this.regleVitesseRaquette.setPaintLabels (false);
        this.regleVitesseRaquette.addChangeListener (new ChangeListener () {
            public void stateChanged (ChangeEvent e) {
                Pong.jeu.setVitesseRaquette ((double) (Pong.panneau.regleVitesseRaquette.getValue() / 100 ));
        		Pong.panneau.requestFocusInWindow ();
            }
        });
        
        this.regleVitesseXBalle.setMaximum (1000);
        this.regleVitesseXBalle.setValue ((int) (100 * Pong.physique.getVitesseXBalle ()));
        this.regleVitesseXBalle.setPaintTicks (false);
        this.regleVitesseXBalle.setPaintLabels (false);
        this.regleVitesseXBalle.addChangeListener (new ChangeListener () {
        	public void stateChanged (ChangeEvent e) {
        		Pong.physique.setVitesseXBalle ((double) (Pong.panneau.regleVitesseXBalle.getValue () / 100));
        		Pong.panneau.requestFocusInWindow ();
        	}
        });
        
        this.regleVitesseYBalle.setMaximum (1000);
        this.regleVitesseYBalle.setValue ((int) (100 * Pong.physique.getVitesseYBalle ()));
        this.regleVitesseYBalle.setPaintTicks (false);
        this.regleVitesseYBalle.setPaintLabels (false);
        this.regleVitesseYBalle.addChangeListener (new ChangeListener () {
            public void stateChanged (ChangeEvent e) {
                Pong.physique.setVitesseYBalle ((double) (Pong.panneau.regleVitesseYBalle.getValue() / 100));
        		Pong.panneau.requestFocusInWindow ();
            }
        });
        
        this.regleAccelerationBalle.setMaximum (100);
        this.regleAccelerationBalle.setValue ((int) (Pong.physique.getAccelerationBalle () / 100));
        this.regleAccelerationBalle.setPaintTicks (false);
        this.regleAccelerationBalle.setPaintLabels (false);
        this.regleAccelerationBalle.addChangeListener (new ChangeListener () {
            public void stateChanged (ChangeEvent e) {
                Pong.physique.setAccelerationBalle ((double) (Pong.panneau.regleAccelerationBalle.getValue() * 100));
        		Pong.panneau.requestFocusInWindow ();
            }
        });
        
        this.add (this.labelTailleRaquette);
        this.add (this.regleTailleRaquette);
        this.add (this.labelVitesseRaquette);
        this.add (this.regleVitesseRaquette);
        this.add (this.labelVitesseXBalle);
        this.add (this.regleVitesseXBalle);
        this.add (this.labelVitesseYBalle);
        this.add (this.regleVitesseYBalle);
        this.add (this.labelAccelerationBalle);
        this.add (this.regleAccelerationBalle);
        Pong.fenetre.setVisible(true);
    }
    
    public void fermerMenu () {
    	System.out.print ("Test");
    	Pong.fenetre.setVisible(false);
    	this.removeAll ();
    	this.repaint ();
    	Pong.fenetre.setVisible(true);
    }
}
